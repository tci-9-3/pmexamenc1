package com.example.examenc1;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.google.android.material.textfield.TextInputEditText;

public class RectanguloActividad extends AppCompatActivity {
    private TextInputEditText txtBase, txtAltura;
    private TextView txtUsuario, lblCalArea, lblCalPerimetro;
    private Button btnCalcular, btnLimpiar, btnRegresar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_rectangulo_actividad);

        // Inicializacion

        txtBase = findViewById(R.id.txtBase);
        txtAltura = findViewById(R.id.txtAltura);
        txtUsuario = findViewById(R.id.txtUsuario);
        lblCalArea = findViewById(R.id.lblCalArea);
        lblCalPerimetro = findViewById(R.id.lblCalPerimetro);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        Rectangulo rectangulo = new Rectangulo();

        // Recibe el nombre de usuario desde el Intent
        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("persona");
        txtUsuario.setText("Mi nombre es " + nombre);

        // Configura el botón de calcular
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validarCampos()) {
                    int bas = Integer.parseInt(txtBase.getText().toString());
                    int alt = Integer.parseInt(txtAltura.getText().toString());


                    rectangulo.setBase(bas);
                    rectangulo.setAltura(alt);

                    float area = rectangulo.calcularCalcularArea();
                    float perimetro = rectangulo.calcularPagPerimetro();

                    lblCalArea.setText("" + area);
                    lblCalPerimetro.setText("" + perimetro);
                } else {
                    Toast.makeText(RectanguloActividad.this, "Todos los campos son requeridos y deben ser válidos", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // Configura el botón de limpiar
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        // Configura el botón de regresar
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    private boolean validarCampos() {
        return !txtBase.getText().toString().isEmpty() &&
                !txtAltura.getText().toString().isEmpty();
    }

    private void limpiarCampos() {
        txtBase.setText("");
        txtAltura.setText("");
        lblCalArea.setText("");
        lblCalPerimetro.setText("");
    }
}